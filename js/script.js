var posicionActual = 0;
var posicionesMaximas = 2;
let banderaPlus = false;
let banderaminus = false;
var mql = window.matchMedia("(max-width: 768px)");
let ovales = document.querySelectorAll('.group-ovales span');

var imgs = [
  /*0*/{ id: 1 , img: 'assets/png/group-10.png'},
  /*1*/{ id: 2, img: 'assets/png/group-10.png'},
  /*2*/{ id: 3, img: 'assets/png/group-10.png'},
  /*3*/{ id: 4, img: 'assets/png/group-10.png'}

];
var desplegarCarrusel = function(bandera){
  const carrusel = document.getElementById("carrusel");
  let content = "" ;
  let contentReverse = [] ;
  let x= 0;
  if(bandera){
    banderaPlus = true;
    if(banderaminus){
      banderaminus = false;
      posicionActual++;
      banderaPlus = true;
    }
    while (x < posicionesMaximas){
      if( posicionActual <= imgs.length - 1){
        content += (`
          <div class="items animated fast fadeIn">
            <span> ${imgs[posicionActual].id} </span>
            <img src="${imgs[posicionActual].img}">
          </div> 
        `);  
        if(x+1 < posicionesMaximas || posicionesMaximas == 1){
          posicionActual++;
        }
        x++;  
      }else{
        posicionActual = 0;
      }
    }
    carrusel.innerHTML = content;
  }else{
    banderaminus = true;
    if(banderaPlus){
      banderaPlus = false;
      posicionActual--;
      banderaminus = true;
    }
    while (x < posicionesMaximas){
      if( posicionActual >= 0){
        contentReverse.push (`
          <div class="items animated fast fadeIn">
            <span> ${imgs[posicionActual].id} </span>
            <img src="${imgs[posicionActual].img}">
          </div> 
        `);  
        if(x+1 < posicionesMaximas || posicionesMaximas == 1){
          posicionActual--;
          
        }
        x++;  
      }else{
        posicionActual = (imgs.length -1);
      }
    }
    contentReverse.reverse();
    carrusel.innerHTML = contentReverse;
  }
  console.log(posicionActual);
  // ovales[posicionActual].classList.toggle("activo");
  desplegarOvales();
}

var desplegarOvales = function(){
  ovales.forEach(data =>{
    data.classList.remove("activo");
  })
  ovales[posicionActual].classList.add("activo");
}

function matches()
{
    if(mql.matches){
      console.log("si");
      posicionesMaximas = 1;
      desplegarCarrusel(true);
    }else{
      console.log("No");
      posicionesMaximas = 2;
      desplegarCarrusel(true);
    }
    
}
// mql.addListener(matches);
mql.addEventListener("change", () => {
  // this.checkNative();
  matches();
});
// window.onload = desplegarCarrusel;
window.onload = matches;
/*
var ovales = document.querySelectorAll('.group-ovales span')
hola[0].classList.toggle("activo")
hola.forEach((data,index) =>{
  console.log(index);
})
*/

var collapseNavbar = function(){
  console.log("Si llego aqui :v");
  document.getElementById("nav2").style.display = "block";
}
var closeNavbar = function(){
  document.getElementById("nav2").style.display = "none";
}